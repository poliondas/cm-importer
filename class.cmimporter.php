<?php

class CmImporter
{

    public static function init()
    {
        # code ...
        add_action('admin_init', array('CmImporter', 'check_plugin_dependences'));
        add_action('admin_menu', array('CmImporter', 'custom_tools_menu_page'));
        add_action('wp_ajax_cmi_load_pareamento', array('CmImporter', 'load_pareamento'));
        add_action('wp_ajax_nopriv_cmi_load_pareamento', array('CmImporter', 'load_pareamento'));
        add_action('wp_ajax_cmi_add_item', array('CmImporter', 'add_item_csv'));
        add_action('wp_ajax_nopriv_cmi_add_item', array('CmImporter', 'add_item_csv'));
    }

    public static function plugin_activation()
    {
        # code...
    }

    public static function plugin_deactivation()
    {
        # code ...
    }

    public static function check_plugin_dependences()
    {
        # code ...
    }

    public static function desactivate_plugin()
    {
        # code ...
    }

    public static function add_script_custom()
    {
        //wp_enqueue_script('ajax-notification', WP_PLUGIN_URL . '/cm-importer/js/admin.js', array('jquery'), null, true);
    }

    public static function add_admin_script_custom()
    {
        wp_enqueue_script('importer', CMIMPORTER_PLUGIN_URL . '/js/back.js', array('jquery'), null, true);
        wp_enqueue_style('importer', CMIMPORTER_PLUGIN_URL . '/css/back.css', array(), null, 'all');
    }

    /**
     * Adds a submenu page under a custom post type parent.
     */
    public static function custom_tools_menu_page()
    {
        add_submenu_page(
                'tools.php', 'CM Importer', 'CM Importer', 'manage_options', 'books-shortcode-ref', array('CmImporter', 'custom_tools_menu_page_callback')
        );
    }

    /**
     * Display callback for the submenu page.
     */
    public static function custom_tools_menu_page_callback()
    {
        # incluir um php é melhor do que adicionar html
        include CMIMPORTER_PLUGIN_DIR . "/page.php";
    }

    /**
     * Carrega o arquivo CSV e monta duas listas, uma com o cabeçalho do arquivo
     * e outra com os campos personalidos do tipo de post escolhido
     *
     * @param array $options
     */
    public static function load_pareamento($options = array())
    {
        $csvAsArray = self::load_csv($options);
        $csvHeader = $csvAsArray[0];
        $postType = isset($options['post_type']) ? $options['post_type'] : $_POST['post_type'];
        $keys = self::load_custom_fiels($postType);
        $aReturn = array(
            'csvHeader' => $csvHeader,
            'keys' => $keys,
        );
        echo json_encode($aReturn);
        exit;
    }

    /**
     * Carrega o arquivo CSV passado por ou parâmetro ou por upload e mapeia-o
     * para um array
     *
     * @param array $options
     * @return array
     */
    public static function load_csv($options = array())
    {
        if (isset($options['file'])) {
            $tmpName = $options['file'];
        } else {
            if (!isset($_FILES['file']) || $_FILES['file']['type'] !== 'text/csv') {
                exit;
            }
            $tmpName = $_FILES['file']['tmp_name'];
        }
        $csvAsArray = array_map('str_getcsv', file($tmpName));
        if (empty($csvAsArray[0])) {
            exit;
        }
        return $csvAsArray;
    }

    /**
     * Consulta no banco todos os campos personalizados atribuidos ao tipo de
     * post escolhido
     *
     * @global type $wpdb
     * @param string $postType
     * @return array
     */
    public static function load_custom_fiels($postType = 'post')
    {
        global $wpdb;
        $sql = "
            SELECT pm.meta_key
            FROM wp_postmeta pm
            WHERE post_id IN
                (SELECT id
                 FROM wp_posts
                 WHERE post_type = '{$postType}')
            GROUP BY pm.meta_key
            ORDER BY meta_key ASC
        ";
        $rows = $wpdb->get_results($sql);
        $aFields = array();
        foreach ($rows as $row) {
            $aFields[] = $row->meta_key;
        }
        return $aFields;
    }

    /**
     * Insere ou atualiza itens segundo o arquivo CSV e as informações enviadas
     * pelo formulário
     *
     * @param array $options
     */
    public static function add_item_csv($options = array())
    {
        #pegar csv
        $csvAsArray = self::load_csv($options);
        #pegar header do csv
        $csvHeader = array_shift($csvAsArray);
        #pegar campos
        $campos = explode(',', $_POST['campos']);
        if (count($campos) !== count($csvHeader)) {
            exit;
        }
        #pegar as chaves
        $keyCsv = array_search($_POST['key_csv'], $csvHeader);
        $keyPost = $_POST['key_post'];
        #pegar as chaves
        $title = array_search($_POST['title'], $csvHeader);
        #iterar sobre as linhas do csv
        $aAdd = array();
        $aUpdate = array();

        foreach ($csvAsArray as &$line) {
            $r_add_item = self::add_item($line, $csvHeader, $campos, $keyCsv, $keyPost, $title);
            $item = array(
                'cdLocal' => $r_add_item['post']->ID,
                'cdImportacao' => $line[$keyCsv],
                'title' => '',
            );

            if (isset($r_add_item['title'])) {
                $item['title'] = '1-' . $r_add_item['title'];
            } elseif ($title !== false && isset($campos[$title])) {
                $item['title'] = '2-' . $campos[$title];
            } else {
                $item['title'] = '3-' . $r_add_item['post']->post_title;
            }

            if ($r_add_item['action'] === 1) {
                $aUpdate[] = $item;
            } else {
                $aAdd[] = $item;
            }
        }

        $r = array(
            'erros' => array(),
            'errosContador' => 0,
            'data' => array(
                'atualizados' => array(
                    'qtd' => count($aUpdate),
                    'itens' => $aUpdate,
                ),
                'adicionados' => array(
                    'qtd' => count($aAdd),
                    'itens' => $aAdd,
                ),
            ),
        );
        echo json_encode($r);
        exit;
    }

    /**
     * Insere ou atualiza um item do tipo de post escolhido
     * @param array $line
     * @param array $csvHeader
     * @param array $campos
     * @param int $keyCsv
     * @param string $keyPost
     * @param int $title
     * @return array
     */
    public static function add_item($line, $csvHeader, $campos, $keyCsv, $keyPost, $title)
    {
        $status = 1;
        $post = self::find_post_by_meta($line, $keyCsv, $keyPost);

        if (!$post) {
            $status = 2;
            $stTitle = isset($line[$title]) ? $line[$title] : '';
            $post = self::insert_post($stTitle, $keyPost, $line[$keyCsv]);
        }
        $r = self::update_post($line, $campos, $post);
        return array('action' => $status, 'post' => $post, 'title' => $r['title']);
    }

    /**
     * Consulta na base se há algum post do tipo de post escolhido que possua
     * um campo meta com a chave => valor igual a $keyPost => $line[$keyCsv]
     *
     * @param array $line
     * @param int $keyCsv
     * @param string $keyPost
     * @return array
     */
    public static function find_post_by_meta($line, $keyCsv, $keyPost)
    {
        $args = array(
            'post_type' => $_POST['post_type'],
            'meta_key' => $keyPost,
            'meta_value' => $line[$keyCsv],
            'post_status' => array('Publish', 'Draft', 'Future', 'Auto-Draft'),
        );
        $r = get_posts($args);

        if (isset($r[0])) {
            return $r[0];
        }
        return array();
    }

    /**
     * Atualiza uma lista de campos personalizado do item $post
     *
     * @param array $line
     * @param array $campos
     * @param Object $post
     */
    public static function update_post($line, $campos, $post)
    {
        $r = array();
        $i = -1;
        $aCampoPadrao = array(
            'title' => 'post_title',
            'content' => 'post_content',
            'excerpt' => 'post_excerpt',
        );
        foreach ($line as $value) {
            $i++;
            $campo = $campos[$i];
            if (empty($campo)) {
                continue;
            }
            if (isset($aCampoPadrao[$campo])) {
                $my_post = array(
                    'ID' => $post->ID,
                    $aCampoPadrao[$campo] => $value,
                );
                wp_update_post($my_post);
                if ($campo === 'title') {
                    $r['title'] = $value;
                }
            } else {
                update_post_meta($post->ID, $campo, $value, false);
            }
        }

        return $r;
    }

    /**
     * Insere um novo item do tipo de item escolhido
     * Caso '$title' não seja vazio, será usado como título do item
     * Caso '$keyPost' seja igual a 'cm_importer_cd' o campo personalizado
     * 'cm_importer_cd' será criado para servir como código externo
     *
     * @param string $title
     * @param string $keyPost
     * @param string $keyValue
     * @return object
     */
    public static function insert_post($title, $keyPost, $keyValue)
    {
        $args = array(
            'post_type' => $_POST['post_type'],
            'post_status' => 'publish',
        );

        if (!empty($title)) {
            $args['post_title'] = $title;
        }
        $post_id = wp_insert_post($args);
        if (!$post_id) {
            exit;
        }
        $post = get_post($post_id);

        if (!empty($post)) {
            if ($keyPost === 'cm_importer_cd') {
                update_post_meta($post->ID, $keyPost, $keyValue, false);
            }
            return $post;
        }
        return array();
    }

}
