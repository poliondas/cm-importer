<div class="cm-step cm-step-fields card">
    <h2 class="title">Passo 3: Faça o pareamento dos campos</h2>
    <ul>
        <li>Para cada um dos campos encontrados no arquivo CSV, informe qual o campo correspondente para o <strong>Tipo de post</strong> escolhido.</li>
        <li>Caso não queria usar um dos campos do arquivo CSV, selecione <em>Não Usar</em>.</li>
    </ul>
    <div class="cm-row">
        <div class="cm-col">
            <table class="form-table table-fields">
                <thead>
                    <tr class="form-field form-required">
                        <th scope="row">Coluna do CSV</th>
                        <td><strong>Campo do tipo de post</strong></td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="cm-col">
            <table class="form-table table-key">
                <thead>
                    <tr class="form-field form-required">
                        <th colspan="2">Chave para comparação</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <table class="form-table table-title">
                <thead>
                    <tr class="form-field form-required">
                        <th colspan="2">Campo para usar como título do Tipo de post</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <a class="button button-default btn-retroceder">Voltar</a>
    <a class="button button-primary btn-avancar">Avançar</a>
</div>