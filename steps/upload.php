<div class="cm-step cm-step-upload card">
    <h2 class="title">Passo 2: Envie o arquivo CSV</h2>
    <p>Clique no campo abaixo e faça o upload do arquivo no formato <strong>CSV</strong>.</p>
    <form id="cm-step-upload-form">
        <a class="button button-secondary btn-retroceder">Voltar</a>
        <a class="button button-primary btn-upload">
            <input type="file" name="cm_importer_upload" class="cm_importer_upload" accept=".csv">
            Selecione o arquivo CSV
        </a>
    </form>

</div>