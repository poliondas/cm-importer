<div class="cm-step cm-step-pt card">
    <h2 class="title">Passo 1: Escolha o PostType</h2>
    <p>Escolha na lista abaixo qual o <em><u>tipo de post</u></em> que você deseja importar.</p>
    <select class="regular-text" id="cm-step-pt-type">
        <?php
        $p = get_post_types('', 'objects');
        $aPostType = array();
        foreach ($p as $key => $item) {
            $aPostType[] = array(
                'name' => $item->name,
                'label' => $item->label,
            );
        }
        usort($aPostType, function($a, $b) {
            return ($a['label'] < $b['label']) ? -1 : 1;
        });
        foreach ($aPostType as $item) {
            echo "<option value='{$item['name']}'>{$item['label']}</option>";
        }
        ?>
    </select>
    <a class="button button-primary btn-avancar">Avançar</a>
</div>