<?php

/**
 * @package CmImporter
 */
/*
  Plugin Name: Cm Importer
  Depends: .
  Plugin URI: https://bitbucket.org/poliondas/cm-importer/overview
  Description: Importar Post Types a partir de um csv.
  Version: 1.0.0
  Author: Carlos Eduardo Tormina Mateus
  Author URI: https://www.linkedin.com/in/carlos-mateus-52605a9a/
  License: GPLv2 or later
  Text Domain: site
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('CMIMPORTER_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('CMIMPORTER_PLUGIN_URL', WP_PLUGIN_URL . '/cm-importer');
define('CMIMPORTER_PLUGIN_URI', CMIMPORTER_PLUGIN_URL);

register_activation_hook(__FILE__, array('CmImporter', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('CmImporter', 'plugin_deactivation'));

require_once( CMIMPORTER_PLUGIN_DIR . 'class.cmimporter.php' );

add_action('init', array('CmImporter', 'init'));
add_action('wp_enqueue_scripts', array('CmImporter', 'add_script_custom'));
add_action('admin_enqueue_scripts', array('CmImporter', 'add_admin_script_custom'));
