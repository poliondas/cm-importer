/* global ajaxurl, postId */
jQuery(function ($) {
    var cmStep = 1;
    
    function avancar(){
        cmStep++;
        loadStep();
    }
    function retroceder(){
        cmStep--;
        loadStep();
    }
    function loadStep(){
        $(".cm-importer-wrap .cm-step").hide();
        switch(cmStep){
            case 1: loadStepPostType(); break;
            case 2: loadStepCSV(); break;
            case 3: loadStepFields(); break;
            case 4: loadStepAdd(); break;
            default: loadStepPostType(); break;
        }
    }
    function loadStepPostType(){
        $(".cm-importer-wrap .cm-step-pt").fadeIn('slow');
        $("#cm-step-upload-form")[0].reset();
    }
    function loadStepCSV(){
        $("#cm-step-upload-form")[0].reset();
        $(".cm-importer-wrap .cm-step-upload").fadeIn('normal');
    }
    function loadStepFields(){
        loadPareamento();
        $(".cm-importer-wrap .cm-step-fields").fadeIn('normal');
    }
    function loadStepAdd(){
        $(".cm-importer-wrap .cm-step-add").fadeIn('normal');
        addItens();
    }
    function loadPareamento(){
        var file = $(".cm-step-upload .cm_importer_upload")[0].files[0];
        var fd = new FormData();
        fd.append('action', 'cmi_load_pareamento');
        fd.append('post_type', $("#cm-step-pt-type").val());
        fd.append('file', file);
        
        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            contentType: false,
            processData: false,
            data: fd,
            success: function(msg){
                var data = JSON.parse(msg);
                loadHtmlPareamento(data);
            }
        });
    }
    function addItens(){
        var $form = $("#cm-step-upload-form");
        var file = $(".cm-step-upload .cm_importer_upload")[0].files[0];
        var $slCampo = $(".sl-campo");
        var fd = new FormData();
        var aCampos = [];
        
        fd.append('action', 'cmi_add_item');
        fd.append('post_type', $("#cm-step-pt-type").val());
        fd.append('key_csv', $(".sl-key-csv").val());
        fd.append('key_post', $(".sl-key-post").val());
        fd.append('title', $(".sl-title").val());
        fd.append('file', file);
        $slCampo.each(function(i){
            aCampos.push($slCampo.eq(i).val());
        });
        fd.append('campos', aCampos);
        
        jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            contentType: false,
            processData: false,
            data: fd,
            success: function(msg){
                var data = JSON.parse(msg);
                var htmlAdd = '';
                for(var i = 0; i < data.data.adicionados.itens.length; i++){
                    var item = data.data.adicionados.itens[i];
                    htmlAdd += ''
                        + '<tr>'
                        + '<td><a href="/wp-admin/post.php?post=' + item.cdLocal + '&action=edit">' + item.cdLocal + '</a></td>'
                        + '<td><a href="/wp-admin/post.php?post=' + item.cdLocal + '&action=edit">' + item.cdImportacao + '</a></td>'
                        + '<td><a href="/wp-admin/post.php?post=' + item.cdLocal + '&action=edit">' + item.title + '</a></td>'
                        + '</tr>'
                        + ''
                    ;
                }
                var htmlUpdate = '';
                for(var i = 0; i < data.data.atualizados.itens.length; i++){
                    var item = data.data.atualizados.itens[i];
                    htmlUpdate += ''
                        + '<tr>'
                        + '<td><a href="/wp-admin/post.php?post=' + item.cdLocal + '&action=edit">' + item.cdLocal + '</a></td>'
                        + '<td><a href="/wp-admin/post.php?post=' + item.cdLocal + '&action=edit">' + item.cdImportacao + '</a></td>'
                        + '<td><a href="/wp-admin/post.php?post=' + item.cdLocal + '&action=edit">' + item.title + '</a></td>'
                        + '</tr>'
                        + ''
                    ;
                }
                var html = ''
                    + '<p><strong>Pronto!</strong></p>'
                    + '<p>Veja abaixo um resumo de sua importação</p>'
                    + '<div class="cm-row">'
                    + '<div class="cm-col">'
                    + '<table class="wp-list-table widefat fixed striped">'
                    + '<thead>'
                    + '<tr>'
                    + '<th colspan="3" class="col"><strong>Itens adicionados: ' + data.data.adicionados.qtd + '</strong></th>'
                    + '</tr>'
                    + '<tr>'
                    + '<th class="col">Código Local</th>'
                    + '<th class="col">Código Importação</th>'
                    + '<th class="col">Título</th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody id="the-list">'
                    + htmlAdd
                    + '</tbody>'
                    + '</table>'
                    + '</div>'
                    + '<div class="cm-col">'
                    + '<table class="wp-list-table widefat fixed striped">'
                    + '<thead>'
                    + '<tr>'
                    + '<th colspan="3" class="col"><strong>Itens atualizados: ' + data.data.atualizados.qtd + '</strong></th>'
                    + '</tr>'
                    + '<tr>'
                    + '<th class="col">Código Local</th>'
                    + '<th class="col">Código Importação</th>'
                    + '<th class="col">Título</th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody id="the-list">'
                    + htmlUpdate
                    + '</tbody>'
                    + '</table>'
                    + '</div>'
                    + '</div>'
                    + '<a class="button button-primary btn-restart">Realizar uma nova importação</a>'
                    + '';
                $(".cm-step-add-content").html(html);
            }
        });
    }
    
    function loadHtmlPareamento(data)
    {
        var htmlKeys = '';
        for(var i = 0; i < data.keys.length; i++){
            htmlKeys += '<option value="' + data.keys[i] + '">' + data.keys[i] + '</option>';
        }
        loadHtmlPareamentoFields(data, htmlKeys);
        loadHtmlPareamentoKey(data, htmlKeys);
        loadHtmlPareamentoTitle(data, htmlKeys);
    }
    
    function loadHtmlPareamentoFields(data, htmlKeys)
    {
        var htmlKeys2 = '' 
            + '<select class="regular-text sl-campo">'
            + '<option value="">Não Usar</option>' 
            + '<optgroup label="Padrão">' 
            + '<option value="title">Titulo</option>' 
            + '<option value="content">Conteúdo</option>' 
            + '<option value="excerpt">Resumo</option>' 
            + '</optgroup>' 
            + '<optgroup label="Personalizado">'
            + htmlKeys 
            + '</optgroup>' 
            + '</select>';
        var html = '';
        for(var i = 0; i < data.csvHeader.length; i++){
            var item = data.csvHeader[i];
            html += ''
                + '<tr class="form-field form-required">'
                + '<th scope="row">' + item + '</th>'
                + '<td data-pos="'+i+'">' + htmlKeys2 + '</td>'
                + '</tr>'
                + '';
        }
        $('.cm-step-fields .table-fields tbody').html(html);
    }
    function loadHtmlPareamentoKey(data, htmlKeys)
    {
        var htmlPostKeys = '' 
            + '<select class="regular-text sl-key-post">'
            + '<option value="cm_importer_cd">Automático</option>' 
            + '<optgroup label="Campo Personalizado">'
            + htmlKeys 
            + '</optgroup>' 
            + '</select>';
    
        var htmlCsvKeys = '';
        for(var i = 0; i < data.csvHeader.length; i++){
            var item = data.csvHeader[i];
            htmlCsvKeys += '<option value="' + item + '">' + item + '</option>' ;
        }
        htmlCsvKeys = '<select class="regular-text sl-key-csv">' + htmlCsvKeys + '</select>';
        
        var html = ''
                + '<tr class="form-field form-required">'
                + '<th scope="row">Colouna no arquivo CSV</th>'
                + '<td>' + htmlCsvKeys + '</td>'
                + '</tr>'
                + '<tr class="form-field form-required">'
                + '<th scope="row">Campo no Tipo de Post</th>'
                + '<td>' + htmlPostKeys + '</td>'
                + '</tr>'
                + '';
        
        $('.cm-step-fields .table-key tbody').html(html);
    }
    function loadHtmlPareamentoTitle(data, htmlKeys)
    {
        var htmlCsvKeys = '';
        for(var i = 0; i < data.csvHeader.length; i++){
            var item = data.csvHeader[i];
            htmlCsvKeys += '<option value="' + item + '">' + item + '</option>' ;
        }
        htmlCsvKeys = '<select class="regular-text sl-title"><option value="">Não Usar</option>' + htmlCsvKeys + '</select>';
        
        var html = ''
                + '<tr class="form-field form-required">'
                + '<th scope="row">Colouna no arquivo CSV</th>'
                + '<td>' + htmlCsvKeys + '</td>'
                + '</tr>'
                + '';
        
        $('.cm-step-fields .table-title tbody').html(html);
    }
        
    $(".cm-importer-wrap .btn-avancar").on('click', avancar);
    $(".cm-importer-wrap .btn-retroceder").on('click', retroceder);
    $(".cm-importer-wrap .cm-step-upload .cm_importer_upload").on('change', function(){
        var file = this.files[0];
        if(!file){
            return;
        }
        if(file.type !== "text/csv"){
            alert('Formato inválido');
        }
        avancar();
    });
    $(".cm-step-add .btn-add").on('click', function(){
        addItens();
    });
    $(document).on('click',".cm-step-add .btn-restart", function(){
        $("#cm-step-upload-form")[0].reset();
        $('.cm-step-fields .table-title tbody').html('');
        $('.cm-step-fields .table-key tbody').html('');
        $('.cm-step-fields .table-fields tbody').html('');
        $(".cm-step-add-content").html('');
        cmStep = 1;
        loadStep();
    });
});